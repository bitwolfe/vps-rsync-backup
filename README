ABOUT THIS SCRIPT
=================

This bash script was designed to automate and simplify the remote backup
process of duplicity on the VPS.net Rsync backup setup. After your script 
is configured, you can easily backup, restore, verify and clean (either via 
cron or manually) your data without having to remember lots of different 
command options and passphrases.

Most importantly, you can easily backup the script and your gpg key in a
convenient passphrase-encrypted file.  This comes in in handy if/when your
machine ever does go belly up.

Optionally, you can set up an email address where the log file will be sent,
which is useful when the script is used via cron.

Latest version of the code is available at:
http://bitbucket.org/frostshift/vps-rsync-backup/

BEFORE YOU START
================

This script requires user configuration.  Instructions are in
the file itself and should be self-explanatory.  Be sure to replace all the
*foobar* values with your real ones.  Almost every value needs to be
configured in someway.

You can use multiple copies of the script with different settings for different
backup scenarios. It is designed to run as a cron job and will log information
to a text file. Be sure to make the script executable (chmod +x) before you hit 
the gas.

REQUIREMENTS
============

* duplicity
* gpg
* mailx (optional)

COMMON USAGE EXAMPLES
=====================

* View help:
    $ vps-rsync-backup.sh

* Run an incremental backup:
    $ vps-rsync-backup.sh --backup

* Force a one-off full backup:
    $ vps-rsync-backup.sh --full

* Restore your entire backup:
    $ vps-rsync-backup.sh --restore
    You will be prompted for a restore directory

    $ vps-rsync-backup.sh --restore /home/user/restore-folder
    You can also provide a restore folder on the command line.

* Restore a specific file in the backup:
    $ vps-rsync-backup.sh --restore-file
    You will be prompted for a file to restore to the current directory

    $ vps-rsync-backup.sh --restore-file img/mom.jpg
    Restores the file img/mom.jpg to the current directory

    $ vps-rsync-backup.sh --restore-file img/mom.jpg /home/user/i-love-mom.jpg
    Restores the file img/mom.jpg to /home/user/i-love-mom.jpg

* List files in the remote archive
    $ vps-rsync-backup.sh --list-current-files

* Verify the backup
    $ vps-rsync-backup.sh --verify

* Backup the script and gpg key (for safekeeping)
    $ vps-rsync-backup.sh --backup-script

TROUBLESHOOTING
===============

This script attempts to simplify the task of running a duplicity command; if you
are having any problems with the script the first step is to determine if the
script is generating an incorrect command or if duplicity itself is
causing your error.

To see exactly what is happening when you run vps-rsync-backup, head to the bottom
of the user configuration portion of the script and uncomment the `ECHO=$(which
echo)` variable.  This will stop the script from running and will, instead,
output the generated command into your log file. You can then check to see if
what is being generated is causing an error or if it is duplicity causing you
woe.

Thanks to Damon Timm, Mario Santagiuliana and Razvan for the original script.
